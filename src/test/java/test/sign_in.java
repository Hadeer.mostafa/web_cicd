package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import Core.Base;

public class sign_in extends Base{
		
	
	@Test(priority = 1)
	public void SignIn() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		WebElement button = driver.findElement(By.xpath("/html/body/div[1]/header/div/ul/li[2]/a"));
		button.click();

	}
	
	@Test(priority = 2)
	public void emptyField() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		WebElement button = driver.findElement(By.id("btn_login"));
		button.click();

	}
	
	
	@Test(priority = 3)
	public void invalidData() throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		WebElement txt_user_email = driver.findElement(By.id("txt_user_email"));
		txt_user_email.sendKeys("hadeer@gmail.com");
		
		driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		WebElement txt_user_password = driver.findElement(By.id("txt_user_password"));
		txt_user_password.sendKeys("123456");
		
		emptyField();

	}
	
	
	
}
